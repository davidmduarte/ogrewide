class Game {
	var s2d: h2d.Scene;
	var bg: h2d.Bitmap;
	var ogre: Ogre;
	var dentist: Dentist;
	var state: Int;
	/**
	 *-1- Wait sate
	 * 0 - Show help information
	 * 1 - Ogre opens mouth
	 * 2 - Dentiste starts to remove tartaro
	 * 3 - Game play
	 * 4 - Ogre rips the dentist harm
	 * 5 - Game Over
	 */
	var gameState: Int;
	var sec = 0.0;
	var score: h2d.Text;

	public function new(_s2d: h2d.Scene) {
		s2d = _s2d;
		state = 2;
		gameState = 0;

		bg = new h2d.Bitmap(h2d.Tile.fromColor(0xffffff, s2d.width, s2d.height), s2d);
		ogre = new Ogre(s2d);
		dentist = new Dentist(ogre, s2d);

		var font: h2d.Font = hxd.res.DefaultFont.get();
		score = new h2d.Text(font);
		score.x = 10;
		score.y = 10;
		score.setScale(2);
		score.text = "0% Cleaned";
		
		score.textColor = 0xffcc00;
		s2d.addChild(score);
	}

	public function showHelpInformation(): Int {
		var box = new h2d.Bitmap(h2d.Tile.fromColor(0x000000, 400, 200, 0.8), s2d);
		box.x = 30;
		box.y = 160;

		var font: h2d.Font = hxd.res.DefaultFont.get();
		var tf = new h2d.Text(font);
		tf.x = 200;
		tf.y = 40;
		tf.setScale(1.5);
		tf.text = "You are the Dentist.\nClean the Ogre teeths\n(move mouse to move the dentist arm)\n(press mouse left button to clean)\nBe carful with the Ogre gums\n";
		tf.textAlign = Center;
		tf.textColor = 0xffffff;
		box.addChild(tf);

		var interaction = new h2d.Interactive(box.getSize().width, box.getSize().height, box);
		interaction.onClick = function(e: hxd.Event) {
			box.removeChild(interaction);
			s2d.removeChild(box);
			gameState = 1; // goto to open mouth
		}

		return -1;
	}

	public function showGameOver() {
		var box = new h2d.Bitmap(h2d.Tile.fromColor(0x000000, 400, 200, 0.8), s2d);
		box.x = 30;
		box.y = 160;

		var font: h2d.Font = hxd.res.DefaultFont.get();
		var tf = new h2d.Text(font);
		tf.x = 200;
		tf.y = 40;
		tf.setScale(3);
		tf.text = "GAME OVER";
		tf.textAlign = Center;
		tf.textColor = 0xffffff;
		box.addChild(tf);

		var interaction = new h2d.Interactive(box.getSize().width, box.getSize().height, box);
		interaction.onClick = function(e: hxd.Event) {
			box.removeChild(interaction);
			s2d.removeChild(box);
			state = 1;
		}
	}

	public function showWellDone() {
		var box = new h2d.Bitmap(h2d.Tile.fromColor(0x000000, 400, 200, 0.8), s2d);
		box.x = 30;
		box.y = 160;

		var font: h2d.Font = hxd.res.DefaultFont.get();
		var tf = new h2d.Text(font);
		tf.x = 200;
		tf.y = 40;
		tf.setScale(3);
		tf.text = "WELL DONE";
		tf.textAlign = Center;
		tf.textColor = 0xffffff;
		box.addChild(tf);

		var interaction = new h2d.Interactive(box.getSize().width, box.getSize().height, box);
		interaction.onClick = function(e: hxd.Event) {
			box.removeChild(interaction);
			s2d.removeChild(box);
			state = 1;
		}
	}

	public function showScore(val: Int) {
		score.text = Std.string(val) + "% Cleaned";
	}

	public function update(dt: Float): Int {
		switch(gameState) {
			case 0:
				gameState = showHelpInformation();
			case 1:
				gameState = ogre.openMouth(dt);
			case 2:
				gameState = dentist.start(dt);
				if(ogre.pain >= 2) {
					gameState = ogre.closeMouth(dt);
					dentist.looseArm();
				}
			case 3:
				sec += dt;
				if(sec > 2.5) gameState = 4;
			case 4:
				showGameOver();
				gameState = 5;
		}

		if(gameState >= 2 && ogre.pain < 2) {
			var sc = ogre.cleanPercentage();
			showScore(sc);
			if(sc > 85) {
				showWellDone();
				gameState = 5;
			}
		}

		return state;
	}

	public function clear() {
		ogre.clear();
		dentist.clear();
	}
}