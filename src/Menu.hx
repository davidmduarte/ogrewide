class Menu {
	var s2d: h2d.Scene;
	var bg: h2d.Bitmap;
	var startBtn: Array<h2d.Bitmap> = [];
	var state: Int;
	var interaction: h2d.Interactive;

	public function new(_s2d: h2d.Scene) {
		s2d = _s2d;
		state = 1;

		bg = new h2d.Bitmap(hxd.Res.splash.toTile(), s2d);

		var auxBtn = new h2d.Bitmap(hxd.Res.start_0.toTile(), s2d);
		auxBtn.x = 100;
		auxBtn.y = 250;
		startBtn.push(auxBtn);

		auxBtn = new h2d.Bitmap(hxd.Res.start_1.toTile(), s2d);
		auxBtn.x = 100;
		auxBtn.y = 250;
		auxBtn.alpha = 0;
		startBtn.push(auxBtn);
		
		interaction = new h2d.Interactive(startBtn[1].getSize().width, startBtn[1].getSize().height, startBtn[1]);
		interaction.onClick = function(e: hxd.Event) {
			state = 2;
		}
		interaction.onOver = function(e: hxd.Event) {
			startBtn[0].alpha = 0;
			startBtn[1].alpha = 1;
		}
		interaction.onOut = function(e: hxd.Event) {
			startBtn[0].alpha = 1;
			startBtn[1].alpha = 0;
		}

		//hxd.Res.music.play(true);
		hxd.Res._music.play(true);
	}

	public function update(dt: Float): Int {
		return state;
	}

	public function stopMusic() {
		//hxd.Res.music.stop();
		hxd.Res._music.stop();
	}

	public function clear() {
		startBtn[1].removeChild(interaction);
		s2d.removeChild(bg);
		s2d.removeChild(startBtn[0]);
		s2d.removeChild(startBtn[1]);
	}
}