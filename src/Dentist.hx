import hxd.Math;
import h2d.col.Point;

class Dentist {
	var s2d: h2d.Scene;
	var ogre: Ogre;
	var body: h2d.Bitmap;
	var arm: h2d.Bitmap;
	var foreArm: h2d.Bitmap;
	var foreArmEaten: h2d.Bitmap;
	var blocked: Bool;
	var interaction: h2d.Interactive;
	var pivotPoint1: Point;
	var pivotPoint2: Point;
	var armLength: Float;
	var foreArmLength: Float;
	var playing = false;

	public function new(_ogre: Ogre, _s2d: h2d.Scene) {
		ogre = _ogre;
		s2d = _s2d;
		blocked = true;

		pivotPoint1 = new Point(110, 450);
		armLength = 120;
		foreArmLength = 180;

		foreArmEaten = new h2d.Bitmap(hxd.Res.dr_fore_arm_eaten.toTile(), s2d);
		foreArmEaten.alpha = 0;
		foreArmEaten.x = 150;
		foreArmEaten.y = 280;
		
		var drForeArmTile = hxd.Res.dr_fore_arm.toTile();
		drForeArmTile.setCenterRatio(0.3, 0);

		foreArm = new h2d.Bitmap(drForeArmTile, s2d);
		foreArm.x = 280;
		foreArm.y = 220;
		foreArm.rotation = 0.65;

		var drArmTile = hxd.Res.dr_arm.toTile();
		drArmTile.setCenterRatio(0.2, 0.6);
		arm = new h2d.Bitmap(drArmTile, s2d);
		arm.x = 97;
		arm.y = 420;

		body = new h2d.Bitmap(hxd.Res.dr_body.toTile(), s2d);
		body.x = -40;
		body.y = 270;

		interaction = new h2d.Interactive(s2d.width, s2d.height, s2d);
		interaction.onMove = function(e: hxd.Event) {
			if(blocked) return;

			var dx = e.relX - 97;
			var dy = e.relY - 420;
			var dist = Math.sqrt(dx*dx + dy*dy);
			var ang = Math.atan2(dy, dx) + 0.7;

			if(dist >= 110 && dist <= 310 && ang >= -0.35 && ang <= 1.8) {
				arm.rotation = ang;

				foreArm.x = e.relX;
				foreArm.y = e.relY;

				ang += 0.85;
				foreArm.rotation = ang;				
			}

		}
		interaction.onPush = function(e: hxd.Event) {
			if(blocked) return;

			ogre.clearDirt(Std.int(e.relX), Std.int(e.relY));	
			//hxd.Res.on2.play(true);
			hxd.Res._on2.play(true);
		}

		interaction.onRelease =
		interaction.onReleaseOutside = function(e: hxd.Event) {
			if(blocked) return;
			//hxd.Res.on2.stop();
			hxd.Res._on2.stop();
		}
	}

	public function start(dt: Float): Int {
		blocked = false;
		return 2;
	}

	public function looseArm() {
		blocked = false;
		//hxd.Res.on2.stop();
		hxd.Res._on2.stop();
		if(!playing) {
			playing = true;
			//hxd.Res.dr_pain.play();
			hxd.Res._dr_pain.play();
		}

		arm.rotation = 0;
		foreArm.alpha = 0;
		foreArmEaten.alpha = 1;
	}

	public function clear() {
		s2d.removeChild(body);
		s2d.removeChild(arm);
		s2d.removeChild(foreArm);
		s2d.removeChild(foreArmEaten);
		s2d.removeChild(interaction);
	}
}