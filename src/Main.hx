class Main extends hxd.App {
	var state: Int;
	var splash: Splash = null;
	var menu: Menu = null;
	var game: Game = null;

	override function init() {
		super.init();

		var wnd = hxd.Window.getInstance();

		//wnd.displayMode = Fullscreen;
		wnd.resize(480, 640);
		
		//s2d.scaleMode = ScaleMode.LetterBox(480, 270);
		
		// program initializes with the Splash screen
		state = 0;
	}

	override function update(dt: Float) {
		switch state {	

			case 0: // Splash
				if(splash == null) {
					clear();
					splash = new Splash(s2d);
				}

				state = splash.update(dt);

			case 1: // Menu
				if(menu == null) {
					clear();
					menu = new Menu(s2d);
				}

				state = menu.update(dt);

			case 2: // Game
				if(game == null) {
					menu.stopMusic();
					clear();
					game = new Game(s2d);
				}

				state = game.update(dt);
		}
	}

	function clear() {
		if(splash != null) {
			splash.clear();
			splash = null;
		}
		if(menu != null) {
			menu.clear();
			menu = null;
		}

		if(game != null) {
			//game.clear();
			game = null;
		}
	}

	static function main() {
		//hxd.Res.initLocal();	
		hxd.Res.initEmbed();
		new Main();	
	}
}