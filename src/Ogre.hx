import hxd.Math;
import h2d.col.Point;

class Ogre {
	var s2d: h2d.Scene;
	var frames: Array<h2d.Bitmap> = [];
	var tooth: Array<h2d.Bitmap> = [];
	var toothMap: Array<Point> = [
		new Point(197, 292),
		new Point(184, 283),
		new Point(171, 270),
		new Point(174, 250),
		new Point(195, 246),
		new Point(216, 248),
		new Point(234, 252),
		new Point(250, 255),
		new Point(270, 253),
		new Point(295, 251),
		new Point(323, 248),
		new Point(333, 260),
		new Point(330, 275),
		new Point(324, 282),
		new Point(312, 300),
		new Point(167, 481),
		new Point(158, 507),
		new Point(168, 519),
		new Point(169, 530),
		new Point(192, 536),
		new Point(228, 538),
		new Point(259, 539),
		new Point(287, 543),
		new Point(310, 555),
		new Point(326, 553),
		new Point(322, 536),
		new Point(332, 525),
		new Point(332, 506),
		new Point(334, 490)
	];
	
	var curFrame: Int;
	var sec: Float;
	public var pain: Int = 0;
	var openMouthPlaying = false;
	var closeMouthPlaying = false;
	
	public function new(_s2d: h2d.Scene) {
		s2d = _s2d;

		sec = 0;
		curFrame = 0;
		
		// ogre images
		var auxFrame = new h2d.Bitmap(hxd.Res.ogre_0.toTile(), s2d);
		auxFrame.x = 25;
		auxFrame.y = 0;
		frames.push(auxFrame);

		auxFrame = new h2d.Bitmap(hxd.Res.ogre_1.toTile(), s2d);
		auxFrame.alpha = 0;
		auxFrame.x = 25;
		auxFrame.y = 0;
		frames.push(auxFrame);

		auxFrame = new h2d.Bitmap(hxd.Res.ogre_2.toTile(), s2d);
		auxFrame.alpha = 0;
		auxFrame.x = 25;
		auxFrame.y = 10;
		frames.push(auxFrame);

		auxFrame = new h2d.Bitmap(hxd.Res.ogre_3.toTile(), s2d);
		auxFrame.alpha = 0;
		auxFrame.x = 25;
		auxFrame.y = 10;
		frames.push(auxFrame);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_0.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_1.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_2.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_3.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_4.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_5.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_6.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_7.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_8.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_9.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_10.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_11.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_12.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_13.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_14.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_15.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_16.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_17.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_18.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_19.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_20.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_21.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_22.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_23.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_24.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_25.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_26.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_27.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);

		var auxTooth = new h2d.Bitmap(hxd.Res.ogre_dirt_28.toTile(), s2d);
		auxTooth.alpha = 0;
		auxTooth.x = 25;
		auxTooth.y = 10;
		tooth.push(auxTooth);
	}

	public function showDirt() {
		trace("showDirt");
		for(item in tooth) item.alpha = 1;
	}

	public function hideDirt() {
		trace("hideDirt");
		for(item in tooth) item.alpha = 0;
	}

	public function cleanPercentage(): Int {
		var t = 0.0;
		for(item in tooth) t += item.alpha * 10;

		t = 100 / 290 * (290.0 - t);

		return Std.int(t);
	}

	public function openMouth(dt: Float): Int {
		sec += dt;

		if(!openMouthPlaying) {
			openMouthPlaying = true;
			//hxd.Res.open.play();
			hxd.Res._open.play();
		}

		if(sec < 0.2) curFrame = 0;
		else if(sec < 0.4) curFrame = 1;
		else if(sec < 0.6) curFrame = 2;
		else if(sec < 0.8) curFrame = 3;

		for(i in 0...4) {
			frames[i].alpha = 0;
			if(i == curFrame) frames[i].alpha = 1;
		}

		if(curFrame == 3) {
			sec = 0;
			showDirt();
			return 2;
		}

		return 1;
	}

	public function closeMouth(dt: Float): Int {
		sec += dt;

		if(!closeMouthPlaying) {
			closeMouthPlaying = true;
			//hxd.Res.close.play();
			hxd.Res._close.play();
			hideDirt();
		}

		if(sec < 0.1) curFrame = 3;
		else if(sec < 0.2) curFrame = 2;
		else if(sec < 0.3) curFrame = 1;
		else if(sec < 0.4) curFrame = 0;

		for(i in 0...4) {
			frames[i].alpha = 0;
			if(i == curFrame) frames[i].alpha = 1;
		}

		if(curFrame == 0) {

			return 3;
		}

		return 2;
	}

	public function clearDirt(x: Int, y: Int) {
		x -= 25;
		y -= 10;
		
		for(i in 0...29) {
			var dx = x - toothMap[i].x;
			var dy = y - toothMap[i].y;
			var dist = Math.sqrt(dx * dx + dy * dy);

			if(dist < 9) {
				if(tooth[i].alpha > 0) tooth[i].alpha -= 0.1;
				
				return;
			}
		}

		trace("PAIN");
		//hxd.Res.pain.play();
		hxd.Res._pain.play();

		pain += 1;
	}

	public function clear() {
		for(i in 0...4) {
			s2d.removeChild(frames[0]);
		}
	}
}