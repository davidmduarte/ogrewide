class Splash {
	var s2d: h2d.Scene;
	var bg: h2d.Bitmap;
	var state: Int;
	var interaction: h2d.Interactive;

	public function new(_s2d: h2d.Scene) {
		s2d = _s2d;
		state = 0;

		interaction = new h2d.Interactive(s2d.width, s2d.height, s2d);
		interaction.onClick = function(e: hxd.Event) {
			state = 1;
			
		}

		bg = new h2d.Bitmap(hxd.Res.splash.toTile(), s2d);
		bg.alpha = 0;
		
	}

	public function update(dt: Float): Int {
		bg.alpha += 0.4 * dt;
		return state;
	}

	public function clear() {
		s2d.removeChild(bg);
		s2d.removeChild(interaction);

	}
}